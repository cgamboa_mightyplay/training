using UnityEngine;
//using System.Collections;
//using AtentivSDK;
using AtentivSDK;

public class Atentiv_UnitTest : MonoBehaviour {

	
	// Use this for initialization
	void Start () {

		Configuration.Instance.SetUp ();;

		SessionManager.Instance.SubjectId = 5;
		SessionManager.Instance.SessionId = 3;

		Logger.Instance.Configure ();

	}
	// Update is called once per frame
	void Update () {

	}

	void OnGUI() {

		// Make a background box
		GUI.Box(new Rect(50,50,600,600), "Atentiv Unit Test");


		if(GUI.Button(new Rect(60,60,100,20), "Upload EEG")) {
			Application.LoadLevel(1);
		}		

		if(GUI.Button(new Rect(60,90,100,20), "Upload Log")) {
			Application.LoadLevel(2);
		}
	}
}
